package nl.robfaber.dsmr.reader.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.StringTokenizer;

@Data
@Builder
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Volume {
  private BigDecimal value;
  private String unit; //kWh

  public static Volume from(String readout) {
    try {
      StringTokenizer tokenizer = new StringTokenizer(readout, "*", false);
      return Volume.builder()
          .value(new BigDecimal(tokenizer.nextToken()))
          .unit(tokenizer.nextToken())
          .build();
    } catch (NoSuchElementException e) {
      log.warn("Failed to read volume from {}", readout);
      return null;
    }
  }

  public Volume minus(Volume v) {
    assert Objects.equals(unit, v.getUnit());
    return Volume.builder()
        .unit(this.unit)
        .value(this.value.subtract(v.getValue()))
        .build();
  }

  public Volume plus(Volume v) {
    assert Objects.equals(unit, v.getUnit());
    return Volume.builder()
        .unit(this.unit)
        .value(this.value.add(v.getValue()))
        .build();
  }
}
