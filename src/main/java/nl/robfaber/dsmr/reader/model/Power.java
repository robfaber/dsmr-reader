package nl.robfaber.dsmr.reader.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

@Data
@Builder
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Power {
  private BigDecimal value;
  private String unit;

  public static Power from(String readout) {
    try {
      StringTokenizer tokenizer = new StringTokenizer(readout, "*", false);
      return Power.builder()
          .value(new BigDecimal(tokenizer.nextToken()))
          .unit(tokenizer.nextToken())
          .build();
    } catch (NoSuchElementException e) {
      log.warn("Failed to read power from {}", readout);
      return null;
    }
  }
}
