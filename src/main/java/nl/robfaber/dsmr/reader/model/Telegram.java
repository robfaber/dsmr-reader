package nl.robfaber.dsmr.reader.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Telegram {
  private String identification;
  private String crc;
  private String version;
  private ZonedDateTime timestamp;
  private String equipmentIdentifier;
  private Electricity electricityDeliveredToClientTariff1;
  private Electricity electricityDeliveredToClientTariff2;
  private Electricity electricityDeliveredByClientTariff1;
  private Electricity electricityDeliveredByClientTariff2;
  private int tariffIndicator; //1 or 2
  private Electricity actualElectricityUsed;
  private Electricity actualElectricityProduced;
  private int numberOfPowerFailuresInAnyPhases;
  private int numberOfLongPowerFailuresInAnyPhase;
  private int numberOfVoltageSagsInPhaseL1;
  private int numberOfVoltageSagsInPhaseL2;
  private int numberOfVoltageSagsInPhaseL3;
  private int numberOfVoltageSwellsInPhaseL1;
  private int numberOfVoltageSwellsInPhaseL2;
  private int numberOfVoltageSwellsInPhaseL3;
  private String textMessageCode;
  private String textMessage;
  private Voltage instantaneousVoltageL1;
  private Voltage instantaneousVoltageL2;
  private Voltage instantaneousVoltageL3;
  private Current instantaneousCurrentL1;
  private Current instantaneousCurrentL2;
  private Current instantaneousCurrentL3;
  private Power instantaneousPowerUsedL1;
  private Power instantaneousPowerUsedL2;
  private Power instantaneousPowerUsedL3;
  private Power instantaneousPowerProducedL1;
  private Power instantaneousPowerProducedL2;
  private Power instantaneousPowerProducedL3;
  private String deviceType;
  private String gasEquipmentIdentifier;
  private ZonedDateTime gasTimestamp;
  private Volume gasUsage;

  @JsonIgnore
  public Electricity getTotalElectricityUsed() {
    return Electricity.builder()
        .value(getElectricityDeliveredToClientTariff1().getValue()
            .add(getElectricityDeliveredToClientTariff2().getValue()))
        .unit(getElectricityDeliveredToClientTariff1().getUnit())
        .build();
  }

  @JsonIgnore
  public Electricity getTotalElectricityProduced() {
    return Electricity.builder()
        .value(getElectricityDeliveredByClientTariff1().getValue()
            .add(getElectricityDeliveredByClientTariff2().getValue()))
        .unit(getElectricityDeliveredByClientTariff1().getUnit())
        .build();
  }

  public LocalDate getDate() {
    if (timestamp  == null) return null;
    return getTimestamp().toLocalDate();
  }
}
