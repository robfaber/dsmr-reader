package nl.robfaber.dsmr.reader.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

@Data
@Builder
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Current {
  private BigDecimal value;
  private String unit;

  public static Current from(String readout) {
    try {
      StringTokenizer tokenizer = new StringTokenizer(readout, "*", false);
      return Current.builder()
          .value(new BigDecimal(tokenizer.nextToken()))
          .unit(tokenizer.nextToken())
          .build();
    } catch (
        NoSuchElementException e) {
      log.warn("Failed to read current from {}", readout);
      return null;
    }
  }
}
