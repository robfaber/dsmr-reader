package nl.robfaber.dsmr.reader.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

@Data
@Builder
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Electricity {
  private BigDecimal value;
  private String unit; //kWh

  public static Electricity from(String readout) {
    try {
      StringTokenizer tokenizer = new StringTokenizer(readout, "*", false);
      return Electricity.builder()
          .value(new BigDecimal(tokenizer.nextToken()))
          .unit(tokenizer.nextToken())
          .build();
    } catch (NoSuchElementException e) {
      log.warn("Failed to read volume from {}", readout);
      return null;
    }
  }


  public Electricity plus(Electricity e2) {
    assert this.getUnit().equals(e2.getUnit());
    return Electricity.builder()
        .unit(this.getUnit())
        .value(this.getValue().add(e2.getValue()))
        .build();
  }

  public Electricity minus(Electricity e2) {
    assert this.getUnit().equals(e2.getUnit());
    return Electricity.builder()
        .unit(this.getUnit())
        .value(this.getValue().subtract(e2.getValue()))
        .build();
  }
}
