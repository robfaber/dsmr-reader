package nl.robfaber.dsmr.reader;

import lombok.extern.slf4j.Slf4j;
import nl.robfaber.dsmr.reader.model.Current;
import nl.robfaber.dsmr.reader.model.Electricity;
import nl.robfaber.dsmr.reader.model.Power;
import nl.robfaber.dsmr.reader.model.Telegram;
import nl.robfaber.dsmr.reader.model.Voltage;
import nl.robfaber.dsmr.reader.model.Volume;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The reader is reading from an input stream and is reading full DSMR telegram messages.
 * Once complete it will inform the listener of the telegram read.
 * This process continues until there is nothing to read anymore from the stream or the reader is stopped.
 */
@Slf4j
public class Reader implements Runnable {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyMMddHHmmss");//There is an extra one S-Daylight saving active, W not active

  // W = Wintertime +01:00
  // S = Summertime +02:00

  private BufferedReader reader;
  private TelegramListener listener;
  private Telegram telegram = null;
  private boolean running = false;

  public Reader(TelegramListener listener) {
    this.listener = listener;
  }

  public Reader(InputStream in) {
    this.reader = new BufferedReader(new InputStreamReader(in));
  }

  public Reader(InputStream in, TelegramListener listener) {
    this(in);
    this.listener = listener;
  }

  public boolean handleLine(String line) {
    if (line == null || line.length() == 0) {
      return false;
    }

    if (telegram == null) {
      telegram = new Telegram();
    }

    int length = line.length();
    if (line.startsWith("/")) {
      telegram.setIdentification(line.substring(1));

    } else if (line.startsWith("!")) {
      telegram.setCrc(line.substring(1));
      //TODO: Implement CRC check
      return true; //Telegram complete

    } else if (line.startsWith("1-3:0.2.8")) {
      telegram.setVersion(line.substring(10, length - 1));

    } else if (line.startsWith("0-0:1.0.0")) {
      telegram.setTimestamp(readTimestamp(line.substring(10, Math.min(22, length - 1)), line.charAt(22)));

    } else if (line.startsWith("0-0:96.1.1")) {
      telegram.setEquipmentIdentifier(line.substring(11, length - 1));

    } else if (line.startsWith("1-0:1.8.1")) {
      telegram.setElectricityDeliveredToClientTariff1(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("1-0:1.8.2")) {
      telegram.setElectricityDeliveredToClientTariff2(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("1-0:2.8.1")) {
      telegram.setElectricityDeliveredByClientTariff1(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("1-0:2.8.2")) {
      telegram.setElectricityDeliveredByClientTariff2(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("1-0:1.7.0")) {
      telegram.setActualElectricityUsed(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("1-0:2.7.0")) {
      telegram.setActualElectricityProduced(Electricity.from(line.substring(10, length - 1)));

    } else if (line.startsWith("0-0:96.14.0")) {
      telegram.setTariffIndicator(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("0-0:96.7.21")) {
      telegram.setNumberOfPowerFailuresInAnyPhases(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("0-0:96.7.9")) {
      telegram.setNumberOfLongPowerFailuresInAnyPhase(Integer.parseInt(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:99.97.0")) {
//    1-0:99.97.0(1)(0-0:96.7.19)(000101010000W)(0000000394*s)
      //Power failure event log, timestamp, duration

    } else if (line.startsWith("1-0:32.32.0")) {
      telegram.setNumberOfVoltageSagsInPhaseL1(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("1-0:52.32.0")) {
      telegram.setNumberOfVoltageSagsInPhaseL2(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("1-0:72.32.0")) {
      telegram.setNumberOfVoltageSagsInPhaseL3(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("1-0:32.36.0")) {
      telegram.setNumberOfVoltageSwellsInPhaseL1(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("1-0:52.36.0")) {
      telegram.setNumberOfVoltageSwellsInPhaseL2(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("1-0:72.36.0")) {
      telegram.setNumberOfVoltageSwellsInPhaseL3(Integer.parseInt(line.substring(12, length - 1)));

    } else if (line.startsWith("0-0:96.13.1")) {
      telegram.setTextMessageCode(line.substring(12, length - 1));

    } else if (line.startsWith("0-0:96.13.0")) {
      telegram.setTextMessage(line.substring(12, length - 1));

    } else if (line.startsWith("1-0:32.7.0")) {
      telegram.setInstantaneousVoltageL1(Voltage.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:52.7.0")) {
      telegram.setInstantaneousVoltageL2(Voltage.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:72.7.0")) {
      telegram.setInstantaneousVoltageL3(Voltage.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:31.7.0")) {
      telegram.setInstantaneousCurrentL1(Current.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:51.7.0")) {
      telegram.setInstantaneousCurrentL2(Current.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:71.7.0")) {
      telegram.setInstantaneousCurrentL3(Current.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:21.7.0")) {
      telegram.setInstantaneousPowerUsedL1(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:41.7.0")) {
      telegram.setInstantaneousPowerUsedL2(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:61.7.0")) {
      telegram.setInstantaneousPowerUsedL3(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:22.7.0")) {
      telegram.setInstantaneousPowerProducedL1(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:42.7.0")) {
      telegram.setInstantaneousPowerProducedL2(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("1-0:62.7.0")) {
      telegram.setInstantaneousPowerProducedL3(Power.from(line.substring(11, length - 1)));

    } else if (line.startsWith("0-1:24.1.0")) {
      telegram.setDeviceType(line.substring(11, length - 1));

    } else if (line.startsWith("0-1:96.1.0")) {
      telegram.setGasEquipmentIdentifier(line.substring(11, length - 1));

    } else if (line.startsWith("0-1:96.1.0")) {
      telegram.setGasEquipmentIdentifier(line.substring(11, length - 1));

    } else if (line.startsWith("0-1:24.2.1")) {
      telegram.setGasTimestamp(readTimestamp(line.substring(11, 23), line.charAt(23)));
      telegram.setGasUsage(Volume.from(line.substring(26, length - 1)));
    } else {
      log.error("Unreadable line: " + line);
    }
    return false;
  }

  public void run() {
    running = true;
    log.info("Running serial reader");
    while (running) {
      Telegram telegram = nextTelegram();
      if (listener != null) listener.onTelegram(telegram);
    }
    log.info("Reader stopped");
  }

  public void stop() {
    log.info("Stopping serial reader");
    running = false;
  }

  public Telegram nextTelegram() {
    boolean telegramComplete = false;
    do {
      try {
        String line = reader.readLine();
        log.debug("Handling line {}", line);
        telegramComplete = handleLine(line);
      } catch (IOException e) {
        log.error("Failed to read line...", e);
      }
    } while (!telegramComplete);
    Telegram telegram = this.telegram;
    this.telegram = null;
    return telegram;
  }

  public List<Telegram> readTelegrams(int count) {
    return IntStream.range(0, count).mapToObj(i -> nextTelegram()).collect(Collectors.toList());
  }


  private ZonedDateTime readTimestamp(String timestampString, char daylightSaving) {
    try {
      LocalDateTime timestamp = LocalDateTime.parse(timestampString, DATE_TIME_FORMATTER);
      String zonedTimestamp = timestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
      if (daylightSaving == 'W') {
        zonedTimestamp += "+01:00";
      } else if (daylightSaving == 'S') {
        zonedTimestamp += "+02:00";
      } else {
        zonedTimestamp += "+00:00";
      }
      return ZonedDateTime.parse(zonedTimestamp, DateTimeFormatter.ISO_DATE_TIME);
    } catch (DateTimeParseException e) {
      log.warn("Failed to parse timestamp from {}", timestampString);
      return null;
    }
  }
}
