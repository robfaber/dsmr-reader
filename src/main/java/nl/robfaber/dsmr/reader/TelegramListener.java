package nl.robfaber.dsmr.reader;

import nl.robfaber.dsmr.reader.model.Telegram;

public interface TelegramListener {
  void onTelegram(Telegram telegram);
}
