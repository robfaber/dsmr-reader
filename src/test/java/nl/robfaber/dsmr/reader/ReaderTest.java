package nl.robfaber.dsmr.reader;

import nl.robfaber.dsmr.reader.model.Current;
import nl.robfaber.dsmr.reader.model.Electricity;
import nl.robfaber.dsmr.reader.model.Power;
import nl.robfaber.dsmr.reader.model.Telegram;
import nl.robfaber.dsmr.reader.model.Voltage;
import nl.robfaber.dsmr.reader.model.Volume;
import org.junit.Test;

import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZonedDateTime.of;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ReaderTest {

  @Test
  public void shouldReadExampleMessage() {

    final List<Telegram> telegrams = new ArrayList<>();

    InputStream inputStream = getClass().getResourceAsStream("/message-1.txt");
    Reader reader = new Reader(inputStream);

    Telegram telegram = reader.nextTelegram();
    assertThat(telegram.getIdentification(), is("XMX5LGF0010453143498"));
    assertThat(telegram.getCrc(), is("6AAB"));
    assertThat(telegram.getVersion(), is("50")); //v5.0
    assertThat(telegram.getTimestamp(), is(ZonedDateTime.of(LocalDateTime.of(2019, 06, 21, 12, 00, 03), ZoneId.of("+02:00"))));
    assertThat(telegram.getEquipmentIdentifier(), is("4530303532303035333134333439383139"));
    assertThat(telegram.getElectricityDeliveredToClientTariff1(), is(Electricity.builder().value(new BigDecimal("6.942")).unit("kWh").build()));
    assertThat(telegram.getElectricityDeliveredToClientTariff2(), is(Electricity.builder().value(new BigDecimal("15.099")).unit("kWh").build()));
    assertThat(telegram.getElectricityDeliveredByClientTariff2(), is(Electricity.builder().value(new BigDecimal("0.000")).unit("kWh").build()));
    assertThat(telegram.getElectricityDeliveredByClientTariff2(), is(Electricity.builder().value(new BigDecimal("0.000")).unit("kWh").build()));
    assertThat(telegram.getTariffIndicator(), is(2));
    assertThat(telegram.getActualElectricityUsed(), is(Electricity.builder().value(new BigDecimal("0.141")).unit("kW").build()));
    assertThat(telegram.getActualElectricityProduced(), is(Electricity.builder().value(new BigDecimal("0.000")).unit("kW").build()));
    assertThat(telegram.getNumberOfPowerFailuresInAnyPhases(), is(8));
    assertThat(telegram.getNumberOfLongPowerFailuresInAnyPhase(), is(3));
    assertThat(telegram.getNumberOfVoltageSagsInPhaseL1(), is(9));
    assertThat(telegram.getNumberOfVoltageSagsInPhaseL2(), is(6));
    assertThat(telegram.getNumberOfVoltageSagsInPhaseL3(), is(6));
    assertThat(telegram.getNumberOfVoltageSwellsInPhaseL1(), is(0));
    assertThat(telegram.getNumberOfVoltageSwellsInPhaseL2(), is(0));
    assertThat(telegram.getNumberOfVoltageSwellsInPhaseL3(), is(0));
//    assertThat(telegram.getTextMessageCode(), is(""));
    assertThat(telegram.getTextMessage(), is(""));
    assertThat(telegram.getInstantaneousVoltageL1(), is(Voltage.builder().value(new BigDecimal("235.8")).unit("V").build()));
    assertThat(telegram.getInstantaneousVoltageL2(), is(Voltage.builder().value(new BigDecimal("231.5")).unit("V").build()));
    assertThat(telegram.getInstantaneousVoltageL3(), is(Voltage.builder().value(new BigDecimal("229.4")).unit("V").build()));
    assertThat(telegram.getInstantaneousCurrentL1(), is(Current.builder().value(new BigDecimal("0")).unit("A").build()));
    assertThat(telegram.getInstantaneousCurrentL1(), is(Current.builder().value(new BigDecimal("0")).unit("A").build()));
    assertThat(telegram.getInstantaneousCurrentL1(), is(Current.builder().value(new BigDecimal("0")).unit("A").build()));
    assertThat(telegram.getInstantaneousPowerUsedL1(), is(Power.builder().value(new BigDecimal("0.039")).unit("kW").build()));
    assertThat(telegram.getInstantaneousPowerUsedL2(), is(Power.builder().value(new BigDecimal("0.072")).unit("kW").build()));
    assertThat(telegram.getInstantaneousPowerUsedL3(), is(Power.builder().value(new BigDecimal("0.031")).unit("kW").build()));
    assertThat(telegram.getInstantaneousPowerProducedL1(), is(Power.builder().value(new BigDecimal("0.000")).unit("kW").build()));
    assertThat(telegram.getInstantaneousPowerProducedL2(), is(Power.builder().value(new BigDecimal("0.000")).unit("kW").build()));
    assertThat(telegram.getInstantaneousPowerProducedL3(), is(Power.builder().value(new BigDecimal("0.000")).unit("kW").build()));
    assertThat(telegram.getDeviceType(), is("003"));
    assertThat(telegram.getGasEquipmentIdentifier(), is("4730303332353635353737343438323138"));
    assertThat(telegram.getGasTimestamp(), is(of(LocalDateTime.of(2019, 06, 21, 11, 55, 8), ZoneId.of("+01:00"))));
    assertThat(telegram.getGasUsage(), is(Volume.builder().value(new BigDecimal("0.000")).unit("m3").build()));
  }

  @Test
  public void shouldReadExampleData() {

    InputStream inputStream = getClass().getResourceAsStream("/test-data.txt");
    Reader reader = new Reader(inputStream);
    List<Telegram> telegrams = reader.readTelegrams(49);

    assertThat(telegrams.size(), is(49));
  }
}
