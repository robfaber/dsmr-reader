package nl.robfaber.dsmr.reader;

import java.io.FileInputStream;
import java.io.InputStream;

public class ReaderExample {
  public static void main(String[] args) throws Exception {

    InputStream inputStream = new FileInputStream("src/test/resources/test-data.txt");
    Reader reader = new Reader(inputStream, telegram -> {
      System.out.println(telegram);
    });
//    reader.run();

    new Thread(reader).start();
  }
}
