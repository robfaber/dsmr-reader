# dsmr-reader

This library contains a reader for the P1 Dutch smart meter requirements (DSMR).
It can be used to parse the serial messages coming in on the P1 port.

The dsmr-reader converts a continuous stream of information from the dsmr meter into Telegrams.


## Usage

Reading telegrams from an input stream;
```java
import nl.robfaber.dsmr.reader.Reader;import nl.robfaber.dsmr.reader.model.Telegram;


import java.io.FileInputStream;
import java.io.InputStream;

public class ReaderExample {
  public static void main(String[] args) throws Exception {

    InputStream inputStream = new FileInputStream("src/test/resources/test-data.txt");
    Reader reader = new Reader(inputStream);
    Telegram telegram = reader.nextTelegram();
    System.out.println(telegram);
  }
}
```

Using callbacks
```java
import nl.robfaber.dsmr.reader.Reader;


import java.io.FileInputStream;
import java.io.InputStream;

public class ReaderExample {
  public static void main(String[] args) throws Exception {

    InputStream inputStream = new FileInputStream("src/test/resources/test-data.txt");
    Reader reader = new Reader(inputStream, telegram -> {
      System.out.println(telegram);
    });
    reader.run();
  }
}
```

The Reader class extends Runnable so you might also start is as a Thread.
```java
    new Thread(reader).start();
```
 